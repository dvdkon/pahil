// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2019 David Koňařík
#pragma once
#include <stddef.h>
#include <stdint.h>

namespace pahil {

/** A rudimentary allocator for allocating memory near the hooked program's
 * memory (so that JMP with a relative offset can be used) in an RWX page It
 * doesn't offer any way to deallocate memory, as it's mostly used to allocate
 * trampolines once on startup */
class ExecAllocator {
    public:
    ExecAllocator(void *addr, size_t capacity);
    void *allocate(size_t size);
    uintptr_t next_address();

    private:
    void *page;
    size_t capacity;
    size_t size;
};


} // namespace
