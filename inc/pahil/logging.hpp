// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <sstream>
#include <iostream>
#include <functional>

#define LOG pahil::logger += pahil::logger.stream() << std::dec << __FILE__ << ":" << __LINE__ << "\t"
#define LOGM(module_name) pahil::logger += pahil::logger.stream() << std::dec << module_name << ":" << __LINE__ << "\t"
#define LOG_BARE pahil::logger += pahil::logger.stream() << std::dec

namespace pahil {

extern thread_local std::ostringstream *logger_last_stream;
class Logger {
    public:
    std::function<void(std::string message)> msg_handler = [](auto msg) {
        std::cout << msg;
    };
    void operator +=(std::ostream& stream) {
        this->msg_handler(logger_last_stream->str());
    }
    std::ostringstream& stream() {
        if(logger_last_stream == NULL) {
            logger_last_stream = new std::ostringstream();
        }
        logger_last_stream->str("");
        logger_last_stream->clear();
        return *logger_last_stream;
    }
};

extern Logger logger;

} // namespace
