// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <stdint.h>
#include <vector>
#include <optional>
#include <filesystem>
#include "exec_allocator.hpp"
#include "patch.hpp"

namespace pahil {


enum class MemoryPermissions : uint8_t {
    NONE = 0,
    READ = 1 << 0,
    WRITE = 1 << 1,
    EXECUTE = 1 << 2,
};
// TODO: Refactor into a common class?
constexpr MemoryPermissions operator |(MemoryPermissions v1,
                                           MemoryPermissions v2) {
    return static_cast<MemoryPermissions>(
        static_cast<uint8_t>(v1)
        | static_cast<uint8_t>(v2));
}
constexpr MemoryPermissions operator |=(MemoryPermissions &v1,
                                            MemoryPermissions v2) {
    v1 = v1 | v2;
    return v1;
}
constexpr MemoryPermissions operator &(MemoryPermissions v1,
                                           MemoryPermissions v2) {
    return static_cast<MemoryPermissions>(
        static_cast<uint8_t>(v1)
        & static_cast<uint8_t>(v2));
}
constexpr MemoryPermissions operator &=(MemoryPermissions &v1,
                                            MemoryPermissions v2) {
    v1 = v1 & v2;
    return v1;
}

struct MemoryRegion {
    uintptr_t start_addr;
    uintptr_t end_addr;
    MemoryPermissions perms;
    bool is_of_module;
};

struct ModuleShared {
    uintptr_t base_addr;
    std::vector<MemoryRegion> mem_regions;
    /** A name identifying the part of the program (e.g. d3d12.dll or
     *  libc.so). nullopt means the current program's executable */
    std::optional<std::string> name;
    /** A path to the mapped executable */
    std::filesystem::path exec_path;
    std::optional<ExecAllocator> exec_allocator;
};

class Module {
    public:
    /* The address that Pahil "pretends" is the base. The real base could be
     * different on each run, so we need a stable address. It's called the
     * "simulated" base, because "virtual" already refers to hardware address
     * translation */
    uintptr_t simul_base_addr;
    /** Some parts of the Module class should be shared for each actual module,
     *  with each Module instance serving as more of a "module handle". These
     *  shared parts are stored in ModuleShared and saved in
     *  modules_shared_parts */
    ModuleShared *shared;

    Module(uintptr_t simul_base_addr,
            std::optional<std::string> name = std::nullopt);

    /** Gets the names of all loaded modules */
    static std::vector<std::optional<std::string>> get_all_modules();
    /** Gets the path to the current executable */
    static std::filesystem::path get_exec_path();
    /** Gets the name of the module containing the specified real address */
    static std::optional<std::string> name_for_addr(uintptr_t real_addr);

    void init_exec_allocator();
    /** Reads the memory region information for the process into a struct.
     *  Also sets name if it is nullopt */
    std::vector<MemoryRegion> parse_mem_regions();
    /** Finds the base address for the main executable */
    uintptr_t find_base();
    /** Translates a simulated address to a real one */
    uintptr_t translate_addr(uintptr_t simul_addr);
    /** Translates a real address to a simulated one */
    uintptr_t translate_addr_rev(uintptr_t real_addr);
    /** Creates a new patch with the correct address offset */
    Patch patch();
    void unprotect_pages();
    /** Returns whether the current executable filename matches */
    bool is_executable_named(std::string name);
};

/** Shortcut function to hook a function pointer
 *  This isn't part of the Module/Patch API, because those are oriented towards
 *  uses where the module and the offset within that module is known, which is
 *  not ergonomic for just patching functions with known "real" locations */
void hook_funcptr(void *ptr, void* hook, void **orig_func = NULL);

} // namespace
