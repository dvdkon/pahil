// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2019 David Koňařík
#pragma once
#include <stdint.h>
#include <string>
#include <exception>
#include <Zydis/Zydis.h>
#include <asmjit/asmjit.h>

namespace pahil {

class DisassemblerException : public std::exception { };

/** A wrapper over Zydis mostly used to get the length of instructions and make
 * sure no jumps are overwritten by hooks
 * Limited to x86_64 */
class Disassembler {
    public:
    /** Initialises Zydis */
    Disassembler();
    /** Gets the length (in bytes) of an instruction in buffer. Presumes that
     * the buffer is as long as the longest instruction, as its main usecase is
     * to be used in the middle of mapped memory. */
    uint8_t get_instruction_length(const uint8_t *buffer);
    /** Gets the lengths of all instructions in buffer, taking at least
     * min_length bytes into account. It expects buffer to point in the middle
     * of mapped memory, min_length is NOT the buffer's length! */
    uint64_t get_instructions_length(const uint8_t *buffer,
                                     uint64_t min_length);
    /** Processes instructions in the same way as get_instructions_length, but
     * returns their textual representation */
    std::string instructions_to_string(const uint8_t *buffer,
                                       uint64_t min_length);
    /** Returns the mnemonic of the first instruction in the buffer */
    std::string get_mnemonic(const uint8_t *buffer);
    /** Takes instructions that were once at orig_addr and tries to write their
     *  equivalent to a new location using asmjit.
     *  This is definitely not "finished" and doesn't handle many cases, but it
     *  works for most functions and is easily extensible */
    void relocate_instructions(asmjit::x86::Assembler& a, uintptr_t orig_addr,
                               const uint8_t *buffer, uint64_t min_length);

    private:
    ZydisDecoder zdecoder;
    ZydisFormatter zformatter;
};

extern Disassembler disassembler;

} //namespace
