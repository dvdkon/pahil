﻿// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <string>

namespace pahil {

/** Will launch an executable and inject a shared library into it.
 *  On Linux, this will directly call exec(), on Windows it will spawn a new
 *  process */
void launch_with_lib(std::string executable, std::string lib);

} // namespace
