// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <stdint.h>
#include <vector>
#include <memory>
#include <optional>
#include <functional>
#include "pahil/exec_allocator.hpp"
#include <asmjit/asmjit.h>

namespace pahil {

class AsmJitException : public std::exception {
    public:
    AsmJitException(const char *msg) : msg(msg) {}
    const char *what() const noexcept override;
    
    private:
    const char *msg;
};

class AsmJitErrorHandler : public asmjit::ErrorHandler {
    public:
    AsmJitErrorHandler() {}
    virtual void handleError(
            asmjit::Error err, const char *msg, asmjit::BaseEmitter *origin);
};

class PatchHunk {
    public:
    virtual ~PatchHunk() {};
    virtual std::unique_ptr<PatchHunk> clone() = 0;
    /** Will return true and apply the change on success, return false and do
     * nothing on failure */
    virtual bool apply() = 0;
    /** Will return a hunk that does the inverse operation, from new_bytes to
     * prev_bytes */
    virtual std::unique_ptr<PatchHunk> inverse() = 0;
};

class DataPatchHunk : public PatchHunk {
    public:
    uintptr_t address;
    std::vector<uint8_t> prev_bytes;
    std::vector<uint8_t> new_bytes;

    DataPatchHunk(uintptr_t address,
                  std::vector<uint8_t> prev_bytes,
                  std::vector<uint8_t> new_bytes);
    virtual std::unique_ptr<PatchHunk> clone();
    virtual bool apply();
    virtual std::unique_ptr<PatchHunk> inverse();
};

class ExecAllocPatchHunk : public PatchHunk {
    public:
    std::function<std::vector<uint8_t>(uintptr_t)> byte_getter;
    std::function<std::unique_ptr<PatchHunk>(uintptr_t)> dependent_hook;
    std::optional<uintptr_t> address;

    ExecAllocPatchHunk(
            ExecAllocator& allocator,
            std::function<std::vector<uint8_t>(uintptr_t)> byte_getter,
            std::function<std::unique_ptr<PatchHunk>(uintptr_t)>
                dependent_hook);
    virtual std::unique_ptr<PatchHunk> clone();
    virtual bool apply();
    virtual std::unique_ptr<PatchHunk> inverse();

    private:
    ExecAllocator& allocator;
    bool is_inverse;
};

/** Represents a transactional change to the hooked program */
class Patch {
    public:
    ptrdiff_t addr_offset;
    std::vector<std::unique_ptr<PatchHunk>> hunks;
    ExecAllocator& exec_allocator;

    Patch(ptrdiff_t addr_offset, ExecAllocator& exec_allocator);
    Patch(ptrdiff_t addr_offset, ExecAllocator& exec_allocator,
          std::vector<std::reference_wrapper<PatchHunk>> hunks);
    void data(uintptr_t addr,
              std::vector<uint8_t> prev_bytes,
              std::vector<uint8_t> new_bytes);
    /**
     * Hook a function at simulated address, redirecting it to hook. The hooks
     * should return a pointer. If it is NULL, the original function is called,
     * otherwise the function returns with the pointed-at value as the return.
     * If orig_func is not null, an address that can be treated like the
     * original function is put into it.
     */
    void hook(uintptr_t at_addr, void *hook, void **orig_func = NULL);
    // _real versions use real addresses instead of simulated ones
    void hook_real(void *at_addr, void *hook, void **orig_func = NULL);
    void redir_funcptr(uintptr_t at_addr, void *redir, void **orig_func = NULL);
    void redir_funcptr_real(void *at_addr, void *redir, void **orig_func = NULL);
    bool apply();
    Patch inverse();
};

typedef std::function<void(asmjit::x86::Assembler&)> AsmFunc;

/** Initialises an asmjit Assembler and passes it to a function, which will use
 *  it to emit instructions. The resultant machine code is then copied into a
 *  byte vector and returned */
std::vector<uint8_t> assemble(
        uint64_t base_addr,
        std::optional<uint64_t> relocate_to,
        AsmFunc asmfunc);

/** Creates a machine code trampoline for calling hook_addr (real address)
 *  which will be jumped to from a hook point */
std::vector<uint8_t> assemble_hook_trampoline(
        uintptr_t addr, void *hook_addr, uintptr_t return_addr,
        std::optional<AsmFunc> pre_call,
        std::optional<AsmFunc> post_call,
        std::vector<uint8_t> replaced_code);
/** Creates a machine code trampoline for calling a hooked function */
std::vector<uint8_t> assemble_direct_trampoline(
        uintptr_t addr, uintptr_t redir_addr,
        std::vector<uint8_t> replaced_code);
/** Creates a single relative JMP instruction that will jump to the specified
 *  address */
std::vector<uint8_t> assemble_hook_jmp(uintptr_t at_addr, uintptr_t to_addr,
                                       size_t pad_to);

/** Takes an integer (only uint64_t supported right now) and converts it into a
 *  byte vector (little endian) */
std::vector<uint8_t> int_to_vec(uint64_t val);

} // namespace
