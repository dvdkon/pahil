﻿// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "pahil/launcher.hpp"
#include <stdexcept>

#if defined(__linux__)
#include <unistd.h>
#elif defined(_WIN32)
#include <Windows.h>
#endif

namespace pahil {

void launch_with_lib(std::string executable, std::string lib) {
#if defined(__linux__)
    setenv("LD_PRELOAD", lib.c_str(), 1);
    execlp(executable.c_str(), executable.c_str(), 0);
#elif defined(_WIN32)
    // Based on code from https://en.wikipedia.org/wiki/DLL_injection
    STARTUPINFO startup_info = {0};
    PROCESS_INFORMATION proc_info {0};
    if(!CreateProcess(
        executable.c_str(), NULL, NULL, NULL, FALSE, CREATE_SUSPENDED,
        NULL, NULL, &startup_info, &proc_info)) {
        throw std::runtime_error("Could not launch " + executable);
    }

    char dll_full_path[_MAX_PATH];
    GetFullPathName(lib.c_str(), _MAX_PATH, dll_full_path, NULL);
    void *path_in_proc = VirtualAllocEx(
        proc_info.hProcess, NULL, _MAX_PATH, MEM_COMMIT | MEM_RESERVE,
        PAGE_READWRITE);
    WriteProcessMemory(
        proc_info.hProcess, path_in_proc, dll_full_path, _MAX_PATH, NULL);

    void *load_library_func = GetProcAddress(
        GetModuleHandle("Kernel32"), "LoadLibraryA");
    HANDLE load_thread = CreateRemoteThread(
        proc_info.hProcess, NULL, 0,
        (LPTHREAD_START_ROUTINE)load_library_func, path_in_proc, 0, NULL);
    WaitForSingleObject(load_thread, INFINITE);
    CloseHandle(load_thread);

    ResumeThread(proc_info.hThread);

    CloseHandle(proc_info.hProcess);
    CloseHandle(proc_info.hThread);
#else
#error "Unsupported platform"
#endif
}

}
