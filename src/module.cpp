// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "pahil/module.hpp"
#include <cassert>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <regex>
#include <optional>
#include <map>
#include "pahil/logging.hpp"

#if defined(__linux__)
#include <sys/mman.h>
#elif defined(_WIN32)
#include <Windows.h>
#include <Psapi.h>
#endif

namespace pahil {

static std::map<std::optional<std::string>, ModuleShared> modules_shared_parts;

Module::Module(uintptr_t simul_base_addr,
                 std::optional<std::string> name)
    : simul_base_addr(simul_base_addr) {
    if(!modules_shared_parts.count(name)) {
        ModuleShared shared;
        this->shared = &shared;
        shared.name = name;
        shared.mem_regions = this->parse_mem_regions();
        shared.base_addr = this->find_base();
        // TODO: Get the path corresponding to the name
        shared.exec_path = Module::get_exec_path();
        modules_shared_parts[name] = shared;
    }
    this->shared = &modules_shared_parts[name];
}

void Module::init_exec_allocator() {
    // Allocate memory as close after mapped pages as possible
    size_t alloc_size = 1 << 16;
    uintptr_t alloc_addr = this->shared->base_addr;

    MEMORY_BASIC_INFORMATION region;
    while(true) {
        if(VirtualQuery(reinterpret_cast<void *>(alloc_addr),
                        &region, sizeof(MEMORY_BASIC_INFORMATION)) == 0) {
            throw std::runtime_error(
                "VirtualQuery() failed! " + std::to_string(GetLastError()));
        }

        if(region.State == MEM_FREE && region.RegionSize >= alloc_size) {
            break;
        }
        alloc_addr = reinterpret_cast<uintptr_t>(region.BaseAddress)
            + region.RegionSize;
    }

    this->shared->exec_allocator = ExecAllocator(
        reinterpret_cast<void *>(alloc_addr), alloc_size);
}

uintptr_t hexstr_to_addr(std::string hex) {
    uintptr_t addr;
    std::stringstream stream(hex);
    stream >> std::hex >> addr;
    return addr;
}

#if defined(__linux__)
// The regex object can't be global, because the undefined order of
// initialisation could break it
const char *proc_maps_line_regex =
    "^([[:xdigit:]]+)-([[:xdigit:]]+) ([r-][w-][x-][sp]) [[:xdigit:]]+"
    " [[:xdigit:]]+:[[:xdigit:]]+ [[:digit:]]+ +(?:[^/]*/)*(.*)$";
#endif


std::vector<MemoryRegion> Module::parse_mem_regions() {
#if defined(__linux__)
    std::vector<MemoryRegion> mem_regions;
    std::regex line_regex(proc_maps_line_regex);
    std::fstream maps("/proc/self/maps", std::fstream::in);

    while(true) {
        std::string line;
        std::getline(maps, line);
        if(line.empty()) { break; }
        std::smatch match;
        assert(std::regex_match(line, match, line_regex));
        auto filename = match[4];
        if(!this->name) {
            this->name = filename;
        }
        auto is_of_module = filename == this->name;
        auto start_addr = hexstr_to_addr(match[1]);
        auto end_addr = hexstr_to_addr(match[2]);
        std::string permstr = match[3];
        MemoryPermissions perms = MemoryPermissions::NONE;
        if(permstr[0] == 'r') { perms |= MemoryPermissions::READ; }
        if(permstr[1] == 'w') { perms |= MemoryPermissions::WRITE; }
        if(permstr[2] == 'x') { perms |= MemoryPermissions::EXECUTE; }
        mem_regions.push_back({start_addr, end_addr, perms, is_of_module});
    }
    return mem_regions;
#elif defined(_WIN32)
    std::vector<MemoryRegion> mem_regions;
    auto proc = GetCurrentProcess();
    auto mod_name = this->shared->name ? this->shared->name->c_str() : NULL;
    auto win_mod = GetModuleHandle(mod_name);
    MODULEINFO mod_info;
    if(!GetModuleInformation(proc, win_mod, &mod_info, sizeof(MODULEINFO))) {
        throw std::runtime_error(
            "GetModuleInformation() failed! "
            + std::to_string(GetLastError()));
    }

    uintptr_t addr = reinterpret_cast<uintptr_t>(mod_info.lpBaseOfDll);
    uintptr_t exe_end = addr + mod_info.SizeOfImage;
    do {
        MEMORY_BASIC_INFORMATION region;
        if(VirtualQuery(reinterpret_cast<void *>(addr),
                        &region, sizeof(MEMORY_BASIC_INFORMATION)) == 0) {
            throw std::runtime_error(
                "VirtualQuery() failed! " + std::to_string(GetLastError()));
        }
        uintptr_t start = reinterpret_cast<uintptr_t>(region.BaseAddress);
        uintptr_t end = reinterpret_cast<uintptr_t>(region.BaseAddress)
            + region.RegionSize;

        if(region.State == MEM_COMMIT) {
            MemoryPermissions perms;
            switch(region.Protect) {
                case PAGE_EXECUTE:
                    perms = MemoryPermissions::EXECUTE;
                    break;
                case PAGE_EXECUTE_READ:
                case PAGE_EXECUTE_WRITECOPY:
                    perms = MemoryPermissions::EXECUTE
                        | MemoryPermissions::READ;
                    break;
                case PAGE_EXECUTE_READWRITE:
                    perms = MemoryPermissions::EXECUTE
                        | MemoryPermissions::READ
                        | MemoryPermissions::WRITE;
                    break;
                case PAGE_NOACCESS:
                    perms = MemoryPermissions::NONE;
                    break;
                case PAGE_READONLY:
                    perms = MemoryPermissions::READ;
                    break;
                case PAGE_READWRITE:
                case PAGE_WRITECOPY:
                    perms = MemoryPermissions::READ
                        | MemoryPermissions::WRITE;
                    break;
                default:
                    throw std::runtime_error(
                        "Unknown page permissions! "
                        + std::to_string(region.Protect));
            }

            // true - on Windows we always get only the EXE/DLL's own regions
            mem_regions.push_back({ start, end, perms, true });
        }

        addr = end;
    } while(addr <= exe_end);
    std::sort(mem_regions.begin(), mem_regions.end(),
              [](auto &a, auto &b) { return a.start_addr < b.start_addr; });
    return mem_regions;
#else
#error "Unsupported platform"
#endif
}

std::vector<std::optional<std::string>> Module::get_all_modules() {
#if defined(__linux__)
    std::vector<std::string> names;
    std::regex line_regex(proc_maps_line_regex);
    std::fstream maps("/proc/self/maps", std::fstream::in);

    while(true) {
        std::string line;
        std::getline(maps, line);
        if(line.empty()) { break; }
        std::smatch match;
        assert(std::regex_match(line, match, line_regex));
        auto filename = match[4];
        names.push_back(filename);
    }

    return names;
#else
    auto proc = GetCurrentProcess();
    auto modules = new HMODULE[1024]; // XXX: Dynamically resize on error
    DWORD bytes_used;
    if(!EnumProcessModules(proc, modules, 1024*sizeof(HMODULE), &bytes_used)) {
        throw std::runtime_error(
            "EnumProcessModules() failed! " + std::to_string(GetLastError()));
    }
    auto module_count = bytes_used / sizeof(HMODULE);
    std::vector<std::optional<std::string>> names(module_count);
    for(size_t i = 0; i < module_count; i++) {
        auto module = modules[i];
        char module_filename[_MAX_PATH + 1];
        if(GetModuleFileName(module, module_filename, _MAX_PATH + 1) == 0) {
            throw std::runtime_error(
                "GetModuleFileName failed! " + std::to_string(GetLastError()));
        }
        std::filesystem::path module_path(module_filename);
        // We push just the filename even though the whole path would be more
        // descriptive, since this is what the rest of the code expects
        // TODO: One day refactor this whole file to actually use full paths and
        // use filenames only as aliases
        if(Module::get_exec_path() == module_path) {
            names.push_back(std::nullopt);
        } else {
            names.push_back(module_path.filename().string());
        }
    }
    delete[] modules;
    return names;
#endif
}

std::filesystem::path Module::get_exec_path() {
#if defined(__linux__)
    return std::filesystem::path(realpath("/proc/self/exe", NULL));
#elif defined(_WIN32)
    char module_filename[_MAX_PATH + 1];
    // NULL - current process' executable  file
    if(GetModuleFileName(NULL, module_filename, _MAX_PATH + 1) == 0) {
        throw std::runtime_error(
            "GetModuleFileName failed! " + std::to_string(GetLastError()));
    }
    return std::filesystem::path(module_filename);
#else
#error "Unsupported platform"
#endif
}

std::optional<std::string> Module::name_for_addr(uintptr_t real_addr) {
    for(auto name : Module::get_all_modules()) {
        Module module(0x0, name);
        if(real_addr < module.shared->base_addr) {
            continue;
        }
        for(auto &region : module.shared->mem_regions) {
            if(real_addr >= region.start_addr && real_addr < region.end_addr) {
                return name;
            }
        }
    }
    throw std::runtime_error(
        "No module for address " + std::to_string(real_addr));
}

uintptr_t Module::find_base() {
    for(auto region : this->shared->mem_regions) {
        if(region.is_of_module) {
            return region.start_addr;
        }
    }
    throw std::runtime_error(
        "pahil::Module::find_base() failed! No memory regions of module");
}

uintptr_t Module::translate_addr(uintptr_t simul_addr) {
    return simul_addr - this->simul_base_addr + this->shared->base_addr;
}

uintptr_t Module::translate_addr_rev(uintptr_t real_addr) {
    return real_addr - this->shared->base_addr + this->simul_base_addr;
}

Patch Module::patch() {
    if(this->shared->exec_allocator == std::nullopt) {
        // This restriction isn't necessary, we just need to move the check into
        // Patch itself
        throw std::runtime_error(
            "Can't create patch without runtime allocator!");
    }
    return Patch(this->shared->base_addr - this->simul_base_addr,
                 *this->shared->exec_allocator);
}

void Module::unprotect_pages() {
    for(auto region : this->shared->mem_regions) {
        if(!region.is_of_module) { continue; }
        if((region.perms & MemoryPermissions::WRITE)
            == MemoryPermissions::NONE) {
#if defined(__linux__)
            int prot = PROT_WRITE;
            if((region.perms & MemoryPermissions::READ)
                != MemoryPermissions::NONE) {
                prot |= PROT_READ;
            }
            if((region.perms & MemoryPermissions::EXECUTE)
                != MemoryPermissions::NONE) {
                prot |= PROT_EXEC;
            }
            assert(mprotect(reinterpret_cast<void *>(region.start_addr),
                region.end_addr - region.start_addr,
                prot) == 0);
#elif defined(_WIN32)
            unsigned long prot;
            if((region.perms & MemoryPermissions::EXECUTE)
                != MemoryPermissions::NONE) {
                prot = PAGE_EXECUTE_READWRITE;
            } else {
                prot = PAGE_READWRITE;
            }
            unsigned long old_prot;
            if(VirtualProtect(reinterpret_cast<void *>(region.start_addr),
                region.end_addr - region.start_addr,
                prot, &old_prot) == 0) {
                throw std::runtime_error(
                    "VirtualProtect() failed! "
                    + std::to_string(GetLastError()));
            }
#else
#error "Unsupported platform"
#endif
        }
    }

}

bool Module::is_executable_named(std::string name_check) {
    return this->shared->exec_path.filename() == name_check;
}

void hook_funcptr(void *ptr, void* hook, void **orig_func) {
    auto addr = reinterpret_cast<uintptr_t>(ptr);
    auto module_name = Module::name_for_addr(addr);
    Module module(0x0, module_name);
    auto p = module.patch();
    p.hook_real(ptr, hook, orig_func);
    p.apply();
}

} // namespace
