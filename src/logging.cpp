// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2019 David Koňařík
#include "pahil/logging.hpp"

namespace pahil {

thread_local std::ostringstream *logger_last_stream = NULL;
Logger logger;

} // namespace
