// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "pahil/patch.hpp"
#include <cstring>
#include <cassert>
#include "asmjit/x86/x86assembler.h"
#include "pahil/logging.hpp"
#include "pahil/disassembler.hpp"

#include <stdio.h>

using namespace asmjit;

namespace pahil {

const char *AsmJitException::what() const noexcept {
    return this->msg;
}

void AsmJitErrorHandler::handleError(
        asmjit::Error err, const char *msg, asmjit::BaseEmitter *origin) {
    throw AsmJitException(msg);
}

DataPatchHunk::DataPatchHunk(uintptr_t address,
                             std::vector<uint8_t> prev_bytes,
                             std::vector<uint8_t> new_bytes)
        : address(address), prev_bytes(prev_bytes), new_bytes(new_bytes) {}

std::unique_ptr<PatchHunk> DataPatchHunk::clone() {
    return std::make_unique<DataPatchHunk>(*this);
}

bool DataPatchHunk::apply() {
    assert(this->prev_bytes.size() == this->new_bytes.size());
    if(memcmp(reinterpret_cast<void *>(this->address),
              this->prev_bytes.data(),
              this->prev_bytes.size()) != 0) {
        return false; // Actual memory doesn't match prev_bytes
    }
    memcpy(reinterpret_cast<void *>(this->address),
           this->new_bytes.data(),
           this->new_bytes.size());
    return true;
}

std::unique_ptr<PatchHunk> DataPatchHunk::inverse() {
    return std::unique_ptr<PatchHunk>(new DataPatchHunk {
        this->address,
        this->new_bytes,
        this->prev_bytes,
    });
}

ExecAllocPatchHunk::ExecAllocPatchHunk(
        ExecAllocator& allocator,
        std::function<std::vector<uint8_t>(uintptr_t)> byte_getter,
        std::function<std::unique_ptr<PatchHunk>(uintptr_t)> dependent_hook)
        : byte_getter(byte_getter), dependent_hook(dependent_hook),
          address({}), allocator(allocator), is_inverse(false) {}

std::unique_ptr<PatchHunk> ExecAllocPatchHunk::clone() {
    return std::make_unique<ExecAllocPatchHunk>(*this);
}

bool ExecAllocPatchHunk::apply() {
    if(this->is_inverse) { return true; }
    auto bytes = this->byte_getter(this->allocator.next_address());
    void *mem = this->allocator.allocate(bytes.size());
    memcpy(mem, bytes.data(), bytes.size());
    this->address = reinterpret_cast<uintptr_t>(mem);
    auto dep_hook = this->dependent_hook(*this->address);
    if(dep_hook != NULL) {
        if(!dep_hook->apply()) {
            return false;
        }
    }
    return true;
}

std::unique_ptr<PatchHunk> ExecAllocPatchHunk::inverse() {
    // We can't really "deallocate" the memory, so inverse will have to be a
    // noop here. However, double inverse should still return a patch hunk
    // equivalent to this.
    std::unique_ptr<ExecAllocPatchHunk> inv(new ExecAllocPatchHunk(*this));
    inv->is_inverse = !inv->is_inverse;
    return inv;
}

Patch::Patch(ptrdiff_t addr_offset, ExecAllocator& exec_allocator)
        : addr_offset(addr_offset), exec_allocator(exec_allocator) {}

Patch::Patch(ptrdiff_t addr_offset, ExecAllocator& exec_allocator,
        std::vector<std::reference_wrapper<PatchHunk>> hunks)
        : addr_offset(addr_offset), exec_allocator(exec_allocator) {
    for(auto hunk : hunks) {
        this->hunks.push_back(hunk.get().clone());
    }
}

void Patch::data(uintptr_t addr,
                 std::vector<uint8_t> prev_bytes,
                 std::vector<uint8_t> new_bytes) {
    this->hunks.push_back(std::make_unique<DataPatchHunk>(
        this->addr_offset + addr,
        prev_bytes,
        new_bytes
    ));
}

void Patch::hook(uintptr_t addr, void *hook, void **orig_func) {
    this->hook_real(reinterpret_cast<void *>(addr + this->addr_offset),
                    hook, orig_func);
}

void Patch::hook_real(void *addr, void *hook, void **orig_func) {
    auto addr_val = reinterpret_cast<uintptr_t>(addr);
    auto addr_ptr = reinterpret_cast<uint8_t *>(addr);
    // TODO: Dehardcode 5
    size_t inst_length = disassembler.get_instructions_length(addr_ptr, 5);
    std::vector<uint8_t> old_instrs(addr_ptr, addr_ptr + inst_length);
    this->hunks.push_back(std::make_unique<ExecAllocPatchHunk>(
        this->exec_allocator,
        [=](uintptr_t tr_addr) {
            return assemble_hook_trampoline(
                tr_addr,
                hook,
                addr_val + 5,
                {}, {},
                old_instrs);
        },
        [=](uintptr_t tr_addr) {
            return std::make_unique<DataPatchHunk>(
                addr_val,
                old_instrs,
                assemble_hook_jmp(addr_val, tr_addr, inst_length));
        }
    ));

    if(orig_func != NULL) {
        // orig_func could point into the hook trampoline, but this is easier
        this->hunks.push_back(std::make_unique<ExecAllocPatchHunk>(
            this->exec_allocator,
            [=](uintptr_t tr_addr) {
                return assemble_direct_trampoline(
                    tr_addr, addr_val + 5, old_instrs);
            },
            [=](uintptr_t tr_addr) {
                *reinterpret_cast<uintptr_t *>(orig_func) = tr_addr;
                return nullptr;
            }
        ));
    }
}

void Patch::redir_funcptr(uintptr_t addr, void *redir, void **orig_func) {
    this->redir_funcptr_real(reinterpret_cast<void *>(addr + this->addr_offset),
                             redir, orig_func);
}

void Patch::redir_funcptr_real(void *addr, void *redir, void **orig_func) {
    auto old_ptr = *reinterpret_cast<uintptr_t *>(addr);
    if(orig_func != NULL) {
        *reinterpret_cast<uintptr_t *>(orig_func) = old_ptr;
    }
    this->hunks.push_back(std::make_unique<ExecAllocPatchHunk>(
        this->exec_allocator,
        [=](uintptr_t tr_addr) {
            return assemble_direct_trampoline(
                tr_addr, reinterpret_cast<uintptr_t>(redir), {});
        },
        [=](uintptr_t tr_addr) {
            return std::make_unique<DataPatchHunk>(
                reinterpret_cast<uintptr_t>(addr),
                int_to_vec(old_ptr),
                int_to_vec(tr_addr));
        }
    ));
}

bool Patch::apply() {
    size_t i = 0;
    for(auto& hunk : this->hunks) {
    if(!hunk->apply()) {
            if(i > 0) {
                for(size_t j = i - 1; j >= 0; j--) {
                    // Applying the inverse should never fail. Failure
                    // indicates either a bug, or some very funky memory
                    assert(this->hunks[j]->inverse()->apply());
                }
            }
            LOG << "Failed to apply patch";
            return false;
        }
        i++;
    }
    return true;
}

Patch Patch::inverse() {
    std::vector<std::reference_wrapper<PatchHunk>> hunks_inverted;
    for(auto& hunk : this->hunks) {
        hunks_inverted.push_back(*hunk->inverse());
    }
    return Patch(this->addr_offset, this->exec_allocator, hunks_inverted);
}

std::vector<uint8_t> assemble(uint64_t base_addr,
                              std::optional<uint64_t> relocate_to,
                              AsmFunc asmfunc) {
    CodeInfo cinfo(ArchInfo::kIdX64);
    cinfo.setBaseAddress(base_addr);
    CodeHolder code;
    code.init(cinfo);
    AsmJitErrorHandler err_handler;
    code.setErrorHandler(&err_handler);
    x86::Assembler a(&code);

    asmfunc(a);

    if(relocate_to) {
        code.relocateToBase(*relocate_to);
    }
    CodeBuffer &buffer = code.textSection()->buffer();
    std::vector<uint8_t> out(buffer.data(), buffer.data() + buffer.size());
    return out;
}

static void direct_trampoline(
        asmjit::x86::Assembler& a, uintptr_t redir_addr,
        std::vector<uint8_t> replaced_code) {
    using namespace asmjit::x86;
    disassembler.relocate_instructions(
        a, redir_addr - replaced_code.size(),
        replaced_code.data(), replaced_code.size());
    a.jmp(imm(redir_addr));
}

std::vector<uint8_t> assemble_hook_trampoline(
        uintptr_t addr, void *hook_addr, uintptr_t return_addr,
        std::optional<AsmFunc> pre_call,
        std::optional<AsmFunc> post_call,
        std::vector<uint8_t> replaced_code) {
    using namespace asmjit::x86;
    return assemble(addr, {}, [=](Assembler& a) {

        Label no_ret = a.newLabel();
        Label no_retval = a.newLabel();

#if defined(__linux__)
        // Some registers need to be caller-preserved
        // See pages 17 and 24 of x86-64-psABI-1.0.pdf
        // TODO: SSE, MMX and FP registers

        std::vector<Gp> preserved_regs = {
            rcx, rdx, rsi, rdi, r8, r9, r10, r11, rax
        };
#elif defined(_WIN32)
        // TODO: SSE, MMX and FP registers
        std::vector<Gp> preserved_regs = {
            /*rax, */rcx, rdx, r8, r9, r10, r11
        };
#else
#error "Unsupported platform"
#endif

        // - Save registers
        for(auto reg : preserved_regs) {
            a.push(reg);
        }

        // - Call 64-bit address
        if(pre_call) { (*pre_call)(a); }
        a.mov(rax, imm(hook_addr));

        // TODO: How to give access to stack params? Right now only register
        // params are accessible :(
        // The usual model of requiring manual delegation to a trampoline of the
        // original function would fix this...
#if defined(__linux__)
        a.call(rax);
#elif defined(_WIN32)
        // Allocate stack scratch space ("register parameter stack area")
        a.sub(rsp, 0x20);
        a.call(rax);
        a.add(rsp, 0x20);
#else
#error "Unsupported platform"
#endif

        if(post_call) { (*post_call)(a); }

        // - If hook return value wasn't NULL, return from hooked function
        a.cmp(rax, 0);
        a.je(no_ret);
        a.cmp(rax, 1);
        a.je(no_retval);
        a.mov(rax, ptr(rax));
        a.bind(no_retval);
        a.add(rsp, preserved_regs.size() * 8); // Discard pushed registers
        a.ret();
        a.bind(no_ret);

        // - Restore registers
        for(size_t i = preserved_regs.size(); i--;) {
            a.pop(preserved_regs[i]);
        }

        // - Run the rest of the function
        direct_trampoline(a, return_addr, replaced_code);
    });
}

std::vector<uint8_t> assemble_direct_trampoline(
        uintptr_t addr, uintptr_t redir_addr,
        std::vector<uint8_t> replaced_code) {
    return assemble(addr, {}, [=](asmjit::x86::Assembler& a) {
        direct_trampoline(a, redir_addr, replaced_code);
    });
}

std::vector<uint8_t> assemble_hook_jmp(uintptr_t at_addr, uintptr_t to_addr,
                                       size_t pad_to) {
    using namespace asmjit::x86;
    return assemble(at_addr, {}, [=](Assembler& a) {
        a.jmp(to_addr);
        for(size_t i = a.offset(); i < pad_to; i++) {
            a.nop();
        }
    });
}

std::vector<uint8_t> int_to_vec(uint64_t val) {
    auto val_ba = reinterpret_cast<uint8_t *>(&val);
    return std::vector<uint8_t>(val_ba, val_ba + sizeof(val));
}

} // namespace
