// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "pahil/disassembler.hpp"
#include <cstddef>
#include <stdexcept>
#include "Zydis/Mnemonic.h"
#include "Zydis/SharedTypes.h"
#include "pahil/logging.hpp"

namespace pahil {

Disassembler::Disassembler() {
    if(!ZYAN_SUCCESS(ZydisDecoderInit(
                          &this->zdecoder,
                          ZYDIS_MACHINE_MODE_LONG_64,
                          ZYDIS_ADDRESS_WIDTH_64))
       || !ZYAN_SUCCESS(ZydisFormatterInit(
               &this->zformatter, ZYDIS_FORMATTER_STYLE_INTEL))) {
        throw DisassemblerException();
    }
}

uint8_t Disassembler::get_instruction_length(const uint8_t *buffer) {
    ZydisDecodedInstruction instruction;
    if(!ZYAN_SUCCESS(ZydisDecoderDecodeBuffer(
            &this->zdecoder, buffer, 64, &instruction))) {
        throw DisassemblerException();
    }
    return instruction.length;
}

uint64_t Disassembler::get_instructions_length(const uint8_t *buffer,
                                               uint64_t min_length) {
    const uint8_t *buf = buffer;
    uint64_t len_all = 0;
    while(buf < buffer + min_length) {
        uint64_t len = this->get_instruction_length(buf);
        len_all += len;
        buf += len;
    }
    return len_all;
}

std::string Disassembler::instructions_to_string(const uint8_t *buffer,
                                                 uint64_t min_length) {
    std::string out;
    uint64_t i = 0;
    while(i < min_length) {
        ZydisDecodedInstruction instruction;
        if(!ZYAN_SUCCESS(ZydisDecoderDecodeBuffer(
               &this->zdecoder, buffer + i, 64, &instruction))) {
            throw DisassemblerException();
        }
        char str[256];
        ZydisFormatterFormatInstruction(
            &this->zformatter, &instruction, str, sizeof(str),
            ZYDIS_RUNTIME_ADDRESS_NONE);
        out.append(str);
        out.append("\n");

        i += instruction.length;
    }
    return out;
}

std::string Disassembler::get_mnemonic(const uint8_t *buffer) {
    ZydisDecodedInstruction instruction;
    if(!ZYAN_SUCCESS(ZydisDecoderDecodeBuffer(
           &this->zdecoder, buffer, 64, &instruction))) {
        throw DisassemblerException();
    }
    auto mnem = ZydisMnemonicGetString(instruction.mnemonic);
    if(mnem == ZYAN_NULL) {
        throw DisassemblerException();
    }
    return std::string(mnem);
}

void Disassembler::relocate_instructions(
        asmjit::x86::Assembler& a, uintptr_t orig_addr, const uint8_t *buffer,
        uint64_t min_length) {
    uint64_t i = 0;
    while(i < min_length) {
        ZydisDecodedInstruction instruction;
        if(!ZYAN_SUCCESS(ZydisDecoderDecodeBuffer(
               &this->zdecoder, buffer + i, 64, &instruction))) {
            throw DisassemblerException();
        }

        if(instruction.mnemonic == ZYDIS_MNEMONIC_JMP) {
            uintptr_t jmp_target = 0;

            auto operand = instruction.operands[0];
            if(operand.type == ZYDIS_OPERAND_TYPE_IMMEDIATE) {
                if(operand.imm.is_relative) {
                    jmp_target = orig_addr + i + instruction.length
                        + operand.imm.value.u;
                } else {
                    jmp_target = operand.imm.value.u;
                }
            }
            if(operand.type == ZYDIS_OPERAND_TYPE_MEMORY) {
                uintptr_t target_ptr = 0;
                if(operand.mem.base == 0) { // TODO: Is this the right way to check?
                    target_ptr = operand.mem.disp.value;
                }
                if(operand.mem.base == ZYDIS_REGISTER_RIP) {
                    // TODO: +1?
                    target_ptr = orig_addr + i + instruction.length + operand.mem.disp.value + 1;
                }

                if(target_ptr != 0) {
                    jmp_target = *reinterpret_cast<uintptr_t *>(target_ptr);
                }
            }

            if(jmp_target == 0) {
                throw std::runtime_error(
                    "relocate_instructions() could not handle jmp: "
                    + this->instructions_to_string(buffer + i, 1));
            }

            //uintptr_t current_addr = (a.codeInfo().baseAddress() + a.offset());

            a.jmp(jmp_target);
        } else if(instruction.mnemonic == ZYDIS_MNEMONIC_JZ
                || instruction.mnemonic == ZYDIS_MNEMONIC_JNZ) {
            // This doesn't support all the branching instructions, because there
            // are too damn many of them! (and I need just these for now)
            uintptr_t jump_target = 0;

            auto operand = instruction.operands[0];
            if(operand.type == ZYDIS_OPERAND_TYPE_IMMEDIATE
                    && operand.imm.is_relative) {
                jump_target = orig_addr + i + instruction.length
                    + operand.imm.value.u;
            }

            if(jump_target == 0) {
                throw std::runtime_error(
                    "relocate_instructions() could not handle branch: "
                    + this->instructions_to_string(buffer + i, 1));
            }

            auto jumping = a.newLabel();
            auto not_jumping = a.newLabel();

            if(instruction.mnemonic == ZYDIS_MNEMONIC_JZ) {
                a.jz(jumping);
            } else if(instruction.mnemonic == ZYDIS_MNEMONIC_JNZ) {
                a.jnz(jumping);
            }
            a.jmp(not_jumping);
            a.bind(jumping);
            a.jmp(jump_target);
            a.bind(not_jumping);
        } else {
            a.embed(buffer + i, instruction.length);
        }

        i += instruction.length;
    }
}

Disassembler disassembler
#if defined(__linux__)
// TODO: Find a way to get rid of these
__attribute__((init_priority(1000)))
#endif
;

} // namespace
