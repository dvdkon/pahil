// This file is part of Pahil and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "pahil/exec_allocator.hpp"
#include <assert.h>

#if defined(__linux__)
#include <sys/mman.h>
#elif defined(_WIN32)
#include <Windows.h>
#endif

#include "pahil/logging.hpp"

namespace pahil {

ExecAllocator::ExecAllocator(void *addr, size_t capacity)
        : capacity(capacity), size(0) {
#if defined(__linux__)
    this->page = mmap(addr, capacity, PROT_READ | PROT_WRITE | PROT_EXEC,
                      MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(this->page != MAP_FAILED);
#elif defined(_WIN32)
    // Round down to Windows allocation granularity
    addr = reinterpret_cast<void *>(
        reinterpret_cast<uintptr_t>(addr) & ~0xFFFF);
    // Give VirtualAlloc a few tries, jsut in case.
    for(size_t i = 0; i < 10; i++) {
        this->page = VirtualAlloc(addr, capacity, MEM_COMMIT | MEM_RESERVE,
                                  PAGE_EXECUTE_READWRITE);
        if(this->page != NULL) {
            break;
        }
        addr = reinterpret_cast<void *>(
                reinterpret_cast<uintptr_t>(addr) + 0x10000);
    }
    if(this->page == NULL) {
        throw std::runtime_error(
            "VirtualAlloc() failed! " + std::to_string(GetLastError()));
    }
#endif
    LOG << "ExecAllocator memory mapped at " << std::hex << this->page;
}

void *ExecAllocator::allocate(size_t size) {
    void *mem = static_cast<uint8_t *>(this->page) + this->size;
    this->size += size;
    assert(this->size <= this->capacity);
    return mem;
}

uintptr_t ExecAllocator::next_address() {
    return reinterpret_cast<uintptr_t>(
        static_cast<uint8_t *>(this->page) + this->size);
}

} // namespace
