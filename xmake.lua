add_repositories("dvdkon-repo git@gitlab.com:dvdkon/xmake-repo.git")
add_requires("zydis", "asmjit")

target("pahil")
	set_kind("static")
	add_files("src/*.cpp")
	add_includedirs("inc", {public = true})
	add_headerfiles("inc/**.hpp")
	add_packages("zydis", "asmjit")
	set_warnings("all")
	set_languages("cxx17")
	set_symbols("debug")
	set_optimize("none")
	if is_plat("linux") then
		-- Produce position independent code, needed for use in shared libs
		add_cxflags("-fPIC")
	end
	if is_plat("windows") then
		add_cxxflags("/EHsc") -- Exceptions
	end
	add_defines("ASMJIT_STATIC")